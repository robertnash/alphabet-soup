from itertools import chain
from typing import List, Tuple


class Puzzle(object):
    def __init__(self, file_path: str):
        """
        A puzzle object with a wordlist that contains words to be searched for
        and a 1d array that represents a wordsearch board.
        """
        self.wordlist: List[str] = []
        self.grid: List[Tuple[str, Tuple[int, int]]] = []
        self.grid_flipped: List[Tuple[str, Tuple[int, int]]] = []
        self.lines: List[List[Tuple[str, Tuple[int, int]]]] = []
        self.rows: int = 0
        self.cols: int = 0

        self.build_grids_wordlist(file_path)
        self.build_rows()
        self.build_cols()
        self.build_diags()

    def build_grids_wordlist(self, file_path: str):
        with open(file_path, "r") as file:
            lines = file.read().splitlines()

        num_rows, num_cols = lines.pop(0).split("x")

        self.rows = int(num_rows)
        self.cols = int(num_cols)

        # builds a 1d array of all letters with dimensions
        for i in range(self.rows):
            line = [
                (letter, (i, index))
                for index, letter in enumerate(lines.pop(0).replace(" ", ""))
            ]
            self.grid += line
            self.grid_flipped += reversed(line)

        # builds wordlist
        for word in lines:
            self.wordlist.append(word.replace(" ", ""))

    def build_rows(self):
        for i in range(0, len(self.grid), self.cols):
            self.lines.append(self.grid[i : i + self.cols])

    def build_cols(self):
        for i in range(self.cols):
            self.lines.append(self.grid[i :: self.cols])

    def build_diags(self):
        min_dim = min(self.rows, self.cols)

        """
        uses range function and combines results to get indexes of diagonals
        from 1d array
        """
        index_of_diagonals = chain(
            range(self.cols - 1),
            range(self.cols - 1, self.cols * self.rows, self.cols),
        )

        """
        uses range function and combines results to get lengths of each
        diagonal row based on dimensions
        """
        lengths_of_diagonals = chain(
            range(1, self.cols), range(self.rows, 0, -1)
        )
        lengths_of_diagonals = map(
            lambda x: x if (x < min_dim) else min_dim, lengths_of_diagonals
        )

        """
        gets diagonal indices by pairing each index with
        appropriate length of the diagonal. Add number of columns to index
        and multiply by the range of the length of the diagonal to get the
        1d indices.
        """
        diagonal = [
            [index + (self.cols - 1) * len for len in range(length)]
            for index, length in zip(index_of_diagonals, lengths_of_diagonals)
        ]

        for line in diagonal:
            self.lines.append([self.grid[index] for index in line])
            self.lines.append([self.grid_flipped[index] for index in line])

    def solve(self):
        """
        Solves wordsearch problem after being built.
        Parses 1d array for words in wordlist.
        If found, it prints the word to stdout with the coordinates on of the
        first letter followed by coordinates of the second letter.
        Solver assumes the word only occurs once in the puzzle.
        """
        for word in self.wordlist:
            reversed_word = word[::-1]
            for line in self.lines:
                string = "".join([letter[0] for letter in line])
                if word in string:
                    index = string.find(word)

                    print(
                        "{} {}:{} {}:{}".format(
                            word,
                            line[index][1][0],
                            line[index][1][1],
                            line[index + len(word) - 1][1][0],
                            line[index + len(word) - 1][1][1],
                        )
                    )
                    break
                elif reversed_word in string:

                    index = string.find(reversed_word)

                    print(
                        "{} {}:{} {}:{}".format(
                            word,
                            line[index + len(word) - 1][1][0],
                            line[index + len(word) - 1][1][1],
                            line[index][1][0],
                            line[index][1][1],
                        )
                    )
                    break

import sys
import getopt
from puzzle import Puzzle


def main(argv):

    try:
        opts, _ = getopt.getopt(argv, "hi:", ["ifile="])
    except Exception:
        print("python wordsearch.py -i <input file>")
        sys.exit(2)

    for opt, arg in opts:
        if opt == "-h":
            print("python wordsearch.py -i <input file>")
            sys.exit()
        elif opt in ("-i", "--ifile"):
            file = arg

    puzzle = Puzzle(file)

    puzzle.solve()


if __name__ == "__main__":
    main(sys.argv[1:])
